# SPDX-License-Identifier: Unlicense
# SPDX-FileCopyrightText: 2022 Freifunk Chemnitz e.V. <info@chemnitz.freifunk.net>
#!/usr/bin/env bash

if [ -z "$1" ]; then
	echo "please provide image file"
	exit 1
fi

if [ -z "$2" ]; then
	echo "please provide IPv6 address"
	exit 1
fi

file=$1
shift 1
while (( "$#" )); do
	dbclient root@"$1" "/etc/init.d/haveged stop; /etc/init.d/alfred stop; killall respondd; echo 3 > /proc/sys/vm/drop_caches"
	scp -S dbclient "$file" root@["$1"]:/tmp/gluon.bin
	dbclient root@"$1" "echo 3 > /proc/sys/vm/drop_caches && sysupgrade /tmp/gluon.bin"
	shift 1
done
