<!-- SPDX-License-Identifier: Unlicense -->
<!-- SPDX-FileCopyrightText: 2022 Freifunk Chemnitz e.V. <info@chemnitz.freifunk.net> -->
# update-ffc-node

small script to semi-automatically update Freifunk Chemnitz nodes

## usage

```
./update.sh /path/to/firmware/file.img 2001:db8::7
```

You can add as many IPv6 addresses as you like. It will iterate over all given IPv6 addresses.

## issues

There is no parameter validation and no error checking.
